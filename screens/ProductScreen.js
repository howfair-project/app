import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, Image, Button } from 'react-native';
import { Camera } from 'expo-camera';
import { StatusBar } from 'expo-status-bar';
import  IconButton  from '../components/utils/IconButton';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';


import { useIsFocused } from '@react-navigation/native';

import {storeToken, getStoredToken, getUsers} from '../components/authentification/AuthentificationApi';

import axios from 'axios';

const baseUrl = 'https://world.openfoodfacts.org/api/v0/product/'; //Url of the api

export function ProductScreen({route, navigation}) {

  const [product, setProduct] = useState("");
  const [modalVisible, setModalVisible] = useState(true);

  const {_product} = route.params;
  

  useEffect(()=> {
    setProduct(_product);
  }, [])

  const ecoImages = [
    {img : require('../assets/img/ecoscore-a.png') , letter : 'a', range : [0 ,  19], style: styles.aScore},
    {img : require('../assets/img/ecoscore-b.png') , letter : 'b', range : [20,  39], style: styles.bScore},
    {img : require('../assets/img/ecoscore-c.png') , letter : 'c', range : [40,  59], style: styles.cScore},
    {img : require('../assets/img/ecoscore-d.png') , letter : 'd', range : [60,  79], style: styles.dScore},
    {img : require('../assets/img/ecoscore-e.png') , letter : 'e', range : [80, 100], style: styles.eScore}
  ]

  const ecoscoreCategory = () => {
    let ecoscore = GetDataFromApi(product.ecoscore_grade);
    let found = ecoImages.find(element => element.letter == ecoscore);
      if (found != null) return (
        <View>
          <Image style={styles.ecoscoreCategory} source={found.img}/>
          <Text style={found.style}>{product.ecoscore_score}</Text>
        </View>
      )
    return (
        <Text>Not found</Text>
    )
  }

  //Product image
 // const urlImage = GetDataFromApi(product.image_url);

  const ProductImage = () => {
    let data = GetDataFromApi(product.image_url);
    if (data != null) {
      return <Image style={styles.urlImage} source={{uri: `${data}`}}/>
    } 
    return <View><Text>Aucune image disponible</Text></View>
  }

  function GetDataFromApi(productData) {
    if(productData == '' || productData == 'unknown' || productData == [] || productData == undefined) return null;
    return productData;
  }

  const ShowDataFromApi = (productData, style) => {
    let dataShow = GetDataFromApi(productData);
    if (dataShow != null) return <Text style={style}>{dataShow}</Text>;
    return <Text style={style}>Aucune donnée</Text>
  }

  const productName = () => {
    return ShowDataFromApi(product.product_name, styles.nameModal);
  }
  const productBrands = () => {
    return ShowDataFromApi(product.brands, styles.brandsModal);
  }
  const productCountries = () => {
    return ShowDataFromApi(product.countries, styles.countriesModal);
  }



  function goBackScanner() {
    setModalVisible(false);
    //this.forceUpdate();
    //navigation.navigate("Home");
    navigation.navigate('Home');
  }

  const isFocused = useIsFocused();

  //Render product
  const renderProduct = () => {
   //if(product != "") 
      return (
          <View style={styles.modalView}>
            <View style={styles.imgView}>
                <ProductImage/>
              <View style={styles.circle}>
                {ecoscoreCategory(product)}
              </View>
            </View>
            <Image style={styles.fusee} source={require('../assets/img/fusee.png')}/>
            <Image style={styles.terre} source={require('../assets/img/terre.png')}/>
            <View style={styles.infoView}>
              {productName}
              {productBrands(product)}
              {productCountries(product)}
              <TouchableOpacity style={styles.buttonAutre} 
                onPress={goBackScanner} >
                <Text style={styles.textbuttonAutre}>SCANNER UN AUTRE PRODUIT</Text>
              </TouchableOpacity>
            </View>
          </View>
      );
  }

  
  
    return (
      <View style={styles.container}>
        <Header/>
        {renderProduct(product)}
       
        <Footer/>
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      backgroundColor: '#FFFFFF',
      flex: 1,
    },
    info: {
      margin: 10,
      fontSize: 32,
    },
    camera: {
      flex: 1,
    },
    buttonContainer: {
      flex: 1,
      backgroundColor: 'transparent',
      borderWidth: 3,
      borderRadius: 20,
      borderColor: '#85D6B5',
      flexDirection: 'row',
      height: 300,
      width: 300,
      position: 'absolute', 
      zIndex: 99,
      top: '20%',
      left: '12%',
    },
    button: {
      flex: 0.1,
      alignSelf: 'flex-end',
      alignItems: 'center',
    },
    text: {
      fontSize: 18,
      color: 'white',
      backgroundColor: 'black',
      borderRadius: 10,
    },
    descriptionScanner: {
      fontSize: 12,
      alignItems: 'center',
      fontWeight: 'bold',
      height: 20,
    },
    titleScanner: {
      fontSize: 26,
      textAlign: 'center',
      fontWeight: 'bold',
      height: 80,
      top:40,
    },
    ecoscoreCategory: {
      height: 23,
      width: 23,
      alignSelf: 'center'
    },
    urlImage: {
      height: 250,
      flex: 0,
      width: 250,
      left: '5%',
      top: '5%',
    },
    modalView: {
      backgroundColor: '#7D00DB',
      flex: 1,
    },
    circle: {
      width: 80,
      height: 80,
      borderRadius: 80 / 2,
      backgroundColor: "#FFFFFF",
      position: 'absolute',
      top: '3%',
      left: '72%',
    },
    fusee: {
      width: 80,
      height: 80,
      position: 'absolute',
      top: '3%',
      left: '87%',
    },
    terre: {
      width: 180,
      height: 180,
      position: 'absolute',
      top: '13%',
      left: '82%',
    },
    imgView: {
      backgroundColor: "#9200FF",
      minHeight: 320,
      maxWidth: 320,
      left:'5%',
      top:'2%',
      borderRadius: 35,
    },
    infoView: {
      top:'3%',
    },
    textModal: {
      color: '#FFFFFF',
    },
    aScore: {
      color: '#3C8554',
      fontSize: 40,
      textAlign: 'center',
    },
    bScore: {
      color: '#60AC0E',
      fontSize: 40,
      textAlign: 'center',
    },
    cScore: {
      color: '#EEAE0E',
      fontSize: 40,
      textAlign: 'center',
    },
    dScore: {
      color: '#FF6F1E',
      fontSize: 40,
      textAlign: 'center',
    },
    eScore: {
      color: '#DF1F1F',
      fontSize: 40,
      textAlign: 'center',
    },
    emptyScore: {
      color: '#A9A9A9',
      fontSize: 40,
      textAlign: 'center',
    },
    nameModal: {
      color: '#FFFFFF',
      fontSize: 19,
      textTransform: 'uppercase',
      height: 30,
    },
    brandsModal: {
      color: '#FFFFFF',
      fontSize: 12,
      height: 30,
    },
    countriesModal: {
      color: '#85D6B5',
      fontSize: 12,
      height: 30,
    },
    buttonAutre: {
      backgroundColor: "#FFFFFF",
      alignItems: 'center',
      alignContent: 'center',
      alignSelf: 'center',
      width: 200,
      borderRadius: 35,
    },
    textbuttonAutre: {
      color: '#9200FF',
      fontSize: 9,
    }
  });