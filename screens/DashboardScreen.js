import { Text, View } from 'react-native';
import { Footer } from '../components/Footer';
import { Header } from "../components/Header";

export function DashboardScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <Text>DashboardScreen</Text>
        <Footer/>
      </View>
    );
  }