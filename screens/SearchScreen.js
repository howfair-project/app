import { Text, View } from 'react-native';
import { Footer } from '../components/Footer';
import { Header } from "../components/Header";

export function SearchScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <Text>SearchScreen</Text>
        <Footer/>
      </View>
    );
  }