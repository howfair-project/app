import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, Image, Button } from 'react-native';
import { Camera } from 'expo-camera';
import { StatusBar } from 'expo-status-bar';
import  IconButton  from '../components/utils/IconButton';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';


import { useIsFocused } from '@react-navigation/native';

import {storeToken, getStoredToken, getUsers} from '../components/authentification/AuthentificationApi';

import axios from 'axios';

const baseUrl = 'https://world.openfoodfacts.org/api/v0/product/'; //Url of the api

export function HomeScreen({ navigation }) {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [scanned, setScanned] = useState(false);
  const [product, setProduct] = useState("");
  const [modalVisible, setModalVisible] = useState(true);



  const isFocused = useIsFocused();



  //Get permissions form user
  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  //Get the api data from the data of the QrCode.
  const handleBarCodeScanned = ({ type, data }) => {
    const url = `${baseUrl}${data}`
    axios.get(url).then((response) => {

      setProduct(response.data.product);
      //var token = getStoredToken();
      //if (token != null) addProduct(GetDataFromApi(product.product_name));
      //navigation.navigate('Product',{_product: product});
      //navigation.navigate('Product')
      navigation.navigate('Product',{_product: product && product});
    });
  };

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>Pas d'accès à la caméra</Text>;
  }

  
    return (
      <View style={styles.container}>
        <Header/>
        <Text style={styles.titleScanner}>SCANNER <Image source={require('../assets/img/feuille.png')}/></Text>
        <Text style={styles.descriptionScanner}>Veuillez scanner le code barre du produit</Text>
        {isFocused && <Camera style={styles.camera} type={type} onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}>
          <View style={styles.buttonContainer}></View>
        </Camera>}
        <Footer/>
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      backgroundColor: '#FFFFFF',
      flex: 1,
    },
    info: {
      margin: 10,
      fontSize: 32,
    },
    camera: {
      flex: 1,
    },
    buttonContainer: {
      flex: 1,
      backgroundColor: 'transparent',
      borderWidth: 3,
      borderRadius: 20,
      borderColor: '#85D6B5',
      flexDirection: 'row',
      height: 300,
      width: 300,
      position: 'absolute', 
      zIndex: 99,
      top: '20%',
      left: '12%',
    },
    button: {
      flex: 0.1,
      alignSelf: 'flex-end',
      alignItems: 'center',
    },
    text: {
      fontSize: 18,
      color: 'white',
      backgroundColor: 'black',
      borderRadius: 10,
    },
    descriptionScanner: {
      fontSize: 12,
      alignItems: 'center',
      fontWeight: 'bold',
      height: 20,
    },
    titleScanner: {
      fontSize: 26,
      textAlign: 'center',
      fontWeight: 'bold',
      height: 80,
      top:40,
    },
    ecoscoreCategory: {
      height: 23,
      width: 23,
      alignSelf: 'center'
    },
    urlImage: {
      height: 250,
      flex: 0,
      width: 250,
      left: '5%',
      top: '5%',
    },
    modalView: {
      backgroundColor: '#7D00DB',
      flex: 1,
    },
    circle: {
      width: 80,
      height: 80,
      borderRadius: 80 / 2,
      backgroundColor: "#FFFFFF",
      position: 'absolute',
      top: '3%',
      left: '72%',
    },
    fusee: {
      width: 80,
      height: 80,
      position: 'absolute',
      top: '3%',
      left: '87%',
    },
    terre: {
      width: 180,
      height: 180,
      position: 'absolute',
      top: '13%',
      left: '82%',
    },
    imgView: {
      backgroundColor: "#9200FF",
      minHeight: 320,
      maxWidth: 320,
      left:'5%',
      top:'2%',
      borderRadius: 35,
    },
    infoView: {
      top:'3%',
    },
    textModal: {
      color: '#FFFFFF',
    },
    aScore: {
      color: '#3C8554',
      fontSize: 40,
      textAlign: 'center',
    },
    bScore: {
      color: '#60AC0E',
      fontSize: 40,
      textAlign: 'center',
    },
    cScore: {
      color: '#EEAE0E',
      fontSize: 40,
      textAlign: 'center',
    },
    dScore: {
      color: '#FF6F1E',
      fontSize: 40,
      textAlign: 'center',
    },
    eScore: {
      color: '#DF1F1F',
      fontSize: 40,
      textAlign: 'center',
    },
    emptyScore: {
      color: '#A9A9A9',
      fontSize: 40,
      textAlign: 'center',
    },
    nameModal: {
      color: '#FFFFFF',
      fontSize: 19,
      textTransform: 'uppercase',
      height: 30,
    },
    brandsModal: {
      color: '#FFFFFF',
      fontSize: 12,
      height: 30,
    },
    countriesModal: {
      color: '#85D6B5',
      fontSize: 12,
      height: 30,
    },
    buttonAutre: {
      backgroundColor: "#FFFFFF",
      alignItems: 'center',
      alignContent: 'center',
      alignSelf: 'center',
      width: 200,
      borderRadius: 35,
    },
    textbuttonAutre: {
      color: '#9200FF',
      fontSize: 9,
    }
  });