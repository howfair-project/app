import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreen } from './screens/HomeScreen';
import { LoginScreen } from './screens/LoginScreen';
import { MapScreen } from './screens/MapScreen';
import { CreateAccountScreen } from './screens/CreateAccountScreen';
import { WishlistScreen } from './screens/WishlishScreen';
import { SearchScreen } from './screens/SearchScreen';
import { DashboardScreen } from './screens/DashboardScreen';
import { ProfilScreen } from './screens/ProfilScreen';
import { ProductScreen } from './screens/ProductScreen';

const Tab = createNativeStackNavigator();

export default function App() {
  
  return (
    <NavigationContainer>
      <Tabs/>
    </NavigationContainer>
  );
}

function Tabs() {
  return (
    <Tab.Navigator style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%'}}>
        <Tab.Screen name="Home" component={HomeScreen} options={{ headerShown: false, unmountOnBlur: true}}/>
        <Tab.Screen name="Login" component={LoginScreen} options={{ headerShown: false}}/>
        <Tab.Screen name="Map" component={MapScreen} options={{ headerShown: false}}/>
        <Tab.Screen name="CreateAccount" component={CreateAccountScreen} options={{ headerShown: false}}/>
        <Tab.Screen name="Wishlist" component={WishlistScreen} options={{headerShown: false}}/>
        <Tab.Screen name="Search" component={SearchScreen} options={{headerShown: false}}/>
        <Tab.Screen name="Dashboard" component={DashboardScreen} options={{headerShown: false}}/>     
        <Tab.Screen name="Profil" component={ProfilScreen} options={{headerShown: false}}/>     
        <Tab.Screen name="Product" component={ProductScreen} options={{headerShown: false}}/>   
    </Tab.Navigator>
  ); 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


{/*<Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="CreateAccount" component={CreateAccountScreen} />
        <Stack.Screen name="Map" component={MapScreen} />
        <Stack.Screen name="Scan" component={ScanScreen} />

        <Tabs />
  </Stack.Navigator>*/}