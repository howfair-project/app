export const colors = {
    white: '#fff',
    black: '#000',
    yellow: '#d9e021',
    green: '#39b54a',
    lightblue: '#29abe2',
    darkblue: '#0071bc',
    purple: '#8327FF',
    lightgrey: '#E5E5E5',
}