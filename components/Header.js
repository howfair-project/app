import { useNavigation } from '@react-navigation/native';
import React from 'react';
import {  StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { colors } from '../theme/colors';

export function Header() {

  const navigation = useNavigation();

  return (
    <View style={styles.container1}>
      <View style={styles.container2}>

        <TouchableOpacity onPress={() => navigation.navigate('Wishlist')}>
          <Image source={require("../assets/HowFair/heart-icon.svg")} style={styles.icon1}></Image>
        </TouchableOpacity>

        <TouchableOpacity>
          <Image source={require("../assets/HowFair/cart-icon.svg")} style={styles.icon2}></Image>
        </TouchableOpacity>
      
      </View>

      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <Image source={require("../assets/HowFair/LogoHowFair.png")} style={styles.logo}></Image>
      </TouchableOpacity>

      <View style={styles.container2}>

        <TouchableOpacity>
        <Image source={require("../assets/HowFair/ring-icon.svg")} style={styles.icon3}></Image>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Profil')}>
        <Image source={require("../assets/HowFair/profile-icon.svg")} style={styles.icon4}></Image>
        </TouchableOpacity>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container1: {
    position: 'absolute',
    top:0,
    flex:1,
    flexDirection:'row',
    justifyContent:"space-around",
    alignItems: 'center',
    width:"100%",
    height: 70,
    backgroundColor:colors.purple,
  },
  container2:{
    flex:1,
    flexDirection:'row',
    justifyContent:"space-around",
    alignItems: 'center',
    height:35,
  },
  logo:{
    width:97,
    height:30,
    resizeMode: "center",
    marginHorizontal:50,
  },
  icon1:{
    width:24.60,
    height:21.90,
  },
  icon2:{
    width:23.11,
    height:21.80,
  },
  icon3:{
    width:21.42,
    height:21.82,
  },
  icon4:{
    width:18.2,
    height:22.15,
  }
});
