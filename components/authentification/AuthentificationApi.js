import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const API_URL = "https://howfair-api.herokuapp.com";

export async function saveToken(key, value) {
    await AsyncStorage.setItem(key, value)
}

export async function getStoredToken() {
    let result = AsyncStorage.getItem('api_token')
    return await result;
}

async function isConnected() {

  console.log(await getStoredToken())
  if (getStoredToken() != null) return true;
  return false;
}

async function getCurrentUserName() {
  if (isConnected()) {
     return parseJwt(await getStoredToken()).username;
  }
  return null;
}

async function getCurrentUser() {
  var token = await getStoredToken();

    var user;

    const config = {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }
    let result = await axios.get(API_URL + "/api/users/" + await getCurrentUserId(), config);

    user = result.data;

    return user;
}

async function getCurrentUserId() {
  if (isConnected()) {
     return parseJwt(await getStoredToken()).id;
  }
  return null;
}

function parseJwt(token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(atob(base64));
}

export async function addProduct(name) {
  
  const users = [
    await getCurrentUser(),
  ];
  const product = { name , users};

  var token = await getStoredToken();
  const config = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  }
  console.log(product);
  await axios.post(API_URL + "/api/products", product);
}

export async function getUsers() {
    var token = await getStoredToken();

    var users;

    const config = {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }
    let result = await axios.get(API_URL + "/api/users", config);

    users = result.data;

    return users;
}

export async function storeToken(email,password) {
  if (email != "" && password != "") {
  /*const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: email,
      password: password,
    })
  }
  await axios.post(API_URL + "/authentication_token", config)
  .then(function (response) {
    console.log(response);
    saveToken('api_token',response.token);
  })*/
  
 
  
  await fetch(API_URL + "/authentication_token", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: email,
      password: password,
    })
  }).then(res => res.json())
  .then(resData => {
    saveToken('api_token', resData.token);
    console.log(parseJwt(resData.token));
  })
  }
}