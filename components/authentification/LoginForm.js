//import { StatusBar } from 'expo-status-bar';
import React , { useState, useEffect } from 'react';
import {AppRegistry, StyleSheet, Text, View, TouchableHighlight, Alert, TextInput, Button} from 'react-native';


import {storeToken, getStoredToken, getUsers, addProduct} from '../authentification/AuthentificationApi';

export function LoginForm() {
    const [email, onChangeEmail] = useState('');
    const [password, onChangePassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [usertoken, setUserToken] = useState('sfgrzgrgsrsrg');
   
    const singin = async() => {
      storeToken(email,password);
      //saveToken('api_token',token);
      
      //var t = getStoredToken();
      console.log(await getStoredToken());
      //console.log(t);
      
      //console.log(await getUsers());
      await addProduct("caca");
      
    }

    return (
      <View contentContainerStyle={styles.container}>
      <Text style={styles.message}>{errorMessage}</Text>

      <Text>email</Text>
      <TextInput
        style={styles.input}
        onChangeText={(text) => onChangeEmail(text)}
        value={email}
        keyboardType="email-address"
      />
      <Text>password</Text>
      <TextInput
        style={styles.input}
        onChangeText={(text) => onChangePassword(text)}
        value={password}
        secureTextEntry
      />
      <Button title={"Connexion"} onPress={singin} />
      {errorMessage ? <Text>{errorMessage}</Text> : null}
    </View>
    );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  message: {
    backgroundColor: 'red',
    padding: 5,
    color: 'white'
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  input: {
    borderRadius: 4,
    flexDirection: 'row',
    padding: 12,
    borderColor: 'black'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
});