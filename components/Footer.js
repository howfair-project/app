import { useNavigation } from '@react-navigation/native';
import React from 'react';
import {  StyleSheet, View, Image, TouchableOpacity, Text } from 'react-native';
import { colors } from '../theme/colors';

export function Footer() {

  const navigation = useNavigation();

  return (
    <View style={styles.container1}>
        <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Home')} >
          <Image source={require("../assets/HowFair/scanner-icon.svg")} style={styles.icon1}></Image>
          <Text style={styles.txt} >Scanner</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Map')} >
          <Image source={require("../assets/HowFair/discover-icon.svg")} style={styles.icon2}></Image>
          <Text style={styles.txt} >Map</Text>
        </TouchableOpacity>
 
        <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Dashboard')} >
          <Image source={require("../assets/HowFair/dashboard-icon.svg")} style={styles.icon3}></Image>
          <Text style={styles.txt} >Dashboard</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Search')} >
          <Image source={require("../assets/HowFair/search-icon.svg")} style={styles.icon4}></Image>
          <Text style={styles.txt} >Search</Text>
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container1: {
    position: 'absolute',
    bottom:0,
    flex:1,
    flexDirection:'row',
    alignItems: 'center',
    width:"100%",
    height: 70,
    backgroundColor:colors.lightgrey,
  },
  btn:{
    width:"25%",
    alignItems: "center",
  },
  txt:{
    marginTop:5,
  },
  icon1:{
    width:26.84,
    height:26.84,
  },
  icon2:{
    marginLeft:5,
    width:23.41,
    height:23.41,
  },
  icon3:{
    width:32,
    height:22.54,
  },
  icon4:{
    width:21.89,
    height:21.89,
  }
});
